// Esto con un .JSON se podria hacer tambien pero de otra forma.

import { Producto } from "../models/producto";

export const ListaProductos: Producto[] = [
    {
        id: "GR1",
        name: "Gree tea",
        price: 3.11,
        quantity: 0,
        totalPrecio: 0,
    },
    {
        id: "SR1",
        name: "Strawberries",
        price: 5.00,
        quantity: 0,
        totalPrecio: 0,
    },
    {
        id: "CF1",
        name: "Coffee",
        price: 11.23,
        quantity: 0,
        totalPrecio: 0,
    }
]

    

