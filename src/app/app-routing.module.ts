import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'productos', pathMatch: 'full' },
  /* { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)}, */
  { path: 'productos', loadChildren: './productos/productos.module#ProductosPageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
