import { Component, OnInit, DoCheck } from '@angular/core';
import { ListaProductos } from "../data/productos";
import { ProductoService } from "../services/productos.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  productos = ListaProductos;
  productosCarrito;
  totalPriceProductos;

  constructor(
    private _productosService: ProductoService
  ) { }

  ngOnInit() {
    /* this.productosCarrito = this._productosService.getCarrito(); */
  }

  //DoCheck se activa cada vez que hay un cambio en la pagina.
  ngDoCheck(){
    this.totalPriceProductos = this._productosService.getTotalPrice();
    this.productosCarrito = this._productosService.getCarrito();
  }
}
