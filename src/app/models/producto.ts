export class Producto {
    /*  
        public id: string;
        public name: string;
        public price: number;
        public quantity: number;
        public totalPrecio: number;

        constructor(id, name, price, quantity, totalPrecio){
            this.id = id;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
            this.totalPrecio = totalPrecio;
        } 
    */
    constructor(
        public id: string,
        public name: string,
        public price: number,
        public quantity: number,
        public totalPrecio: number, 
    ){}    
}