import { Injectable, Input, OnInit } from "@angular/core";
import { Producto } from "../models/producto";
import { ListaProductos } from "../data/productos";

@Injectable()
export class ProductoService{

    carrito = [];
    productos = ListaProductos;
    totalPriceProductos;

    constructor() {
        //Al principio creaba los productos desde el servicio para ser mas eficiente pero no me funciono como esperaba. Luego lo cambie a un archivo aparte.
        /* this.productos = [
            new Producto("GR1", "Gree tea", 3.11, 0, 0),
            new Producto("SR1", "Strawberries", 5.00, 0, 0),
            new Producto("CF1", "Coffee", 11.23, 0, 0),
        ] */
    }
    getProductos(): Array<Producto> {
        console.log("los productos son :", this.productos)
        return this.productos;
    }
    addCarrito(producto) {
        if (producto.quantity === 0) {
            this.carrito.push(producto);
            producto.quantity++
            producto.totalPrecio = producto.price;
            console.log("El carrito esta asi: ", this.carrito);
        }
        else {
            producto.quantity++;
            producto.totalPrecio = producto.price * producto.quantity
            console.log("El carrito esta asi: ", this.carrito);
        }
        // condiciones a partir de compra. (Hay otras formas de hacer esto mismo, tal vez mas eficientes).
        if (producto.id === "GR1") {
            producto.quantity++;
            producto.totalPrecio = producto.price * (producto.quantity / 2);
            console.log("segunda unidad gratis")
        }
        else if (producto.id === "SR1" && producto.quantity >= 3) {
            producto.price = 4.50;
            producto.totalPrecio = producto.price * producto.quantity + 1;
            console.log("Las fresas ahora valen 4.5")
        }
        else if (producto.id === "CF1" && producto.quantity > 2) {
            producto.price = 7.486;
            producto.totalPrecio = producto.price * producto.quantity;
            console.log("el precio total de todos los cafes baja 2/3 partes de 11.23")
        }
        console.log(producto.name, producto.quantity)
    }
    getTotalPrice(){
        this.totalPriceProductos = 0;
        for (let i = 0; i < this.carrito.length; i++) {
            this.totalPriceProductos += this.carrito[i].totalPrecio;
            console.log("1-",this.totalPriceProductos);
        }
        return this.totalPriceProductos;
    }
    getCarrito(){
        return this.carrito;
    }
}