import { Component, OnInit} from '@angular/core';

//Importar modelo Producto no hace falta
/* import { Producto } from "../models/producto"; */

//Importar servicio Productos.
import { ProductoService } from "../services/productos.service";
//Importar Lista de productos.
import { ListaProductos } from "../data/productos";

@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
  /*Mejor no añadir esto porque luego no mostrara el carrito. providers: [ProductoService] */
})
export class ProductosPage implements OnInit {

  productos = ListaProductos;
  totalPriceProductos;
  carrito = [];

  constructor
    (
      private _productosService: ProductoService
    ) 
    {
    //esto si lo dejas en el OnInit tambien funciona.
    this.productos = this._productosService.getProductos();
  }

  ngOnInit() {
    /* this.productos = this._productosService.getProductos(); */
  }
  addCarrito(producto) {
    this._productosService.addCarrito(producto);
  }
  /* Al principio mostraba todo en la pagina de productos para ver si las funciones del servicio funcionaban correctamente.
    ngDoCheck(){
    this.totalPriceProductos = this._productosService.getTotalPrice();
    this.carrito = this._productosService.getCarrito();
  }
  getCarrito(){
    console.log("Productos cargados en el carrito: ", this.carrito); 
  } */
}
